var storage = window.localStorage;

window.onload = function() {
    var titles = document.getElementsByClassName("productTitle");
    var links = document.getElementsByClassName("fa-shopping-cart");

    for(var i = 0; i < links.length; ++i) {
        links[i].onclick = function() {
            storage.setItem('name',titles[i].innerHTML);
        }
    }
}